/*----- PROTECTED REGION ID(SimulatedBiltChStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        SimulatedBiltChStateMachine.cpp
//
// description : State machine file for the SimulatedBiltCh class
//
// project :     Simulated Bilt Channel
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2018
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::SimulatedBiltChStateMachine.cpp
#include <tango/classes/SimulatedBiltCh/SimulatedBiltCh.h>

//================================================================
//  States  |  Description
//================================================================
//  ON      |  
//  OFF     |  
//  FAULT   |  
//  ALARM   |  


namespace SimulatedBiltCh_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : SimulatedBiltCh::is_Current_allowed()
 * Description:  Execution allowed for Current attribute
 */
//--------------------------------------------------------
bool SimulatedBiltCh::is_Current_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedBiltCh::CurrentStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::CurrentStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedBiltCh::CurrentStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::CurrentStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedBiltCh::is_Voltage_allowed()
 * Description:  Execution allowed for Voltage attribute
 */
//--------------------------------------------------------
bool SimulatedBiltCh::is_Voltage_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedBiltCh::VoltageStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::VoltageStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : SimulatedBiltCh::is_On_allowed()
 * Description:  Execution allowed for On attribute
 */
//--------------------------------------------------------
bool SimulatedBiltCh::is_On_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(SimulatedBiltCh::OnStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::OnStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedBiltCh::is_Off_allowed()
 * Description:  Execution allowed for Off attribute
 */
//--------------------------------------------------------
bool SimulatedBiltCh::is_Off_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(SimulatedBiltCh::OffStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::OffStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedBiltCh::is_Reset_allowed()
 * Description:  Execution allowed for Reset attribute
 */
//--------------------------------------------------------
bool SimulatedBiltCh::is_Reset_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::ON ||
		get_state()==Tango::OFF)
	{
	/*----- PROTECTED REGION ID(SimulatedBiltCh::ResetStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::ResetStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedBiltCh::is_SetpointCheck_allowed()
 * Description:  Execution allowed for SetpointCheck attribute
 */
//--------------------------------------------------------
bool SimulatedBiltCh::is_SetpointCheck_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(SimulatedBiltCh::SetpointCheckStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::SetpointCheckStateAllowed
		return false;
	}
	return true;
}


/*----- PROTECTED REGION ID(SimulatedBiltCh::SimulatedBiltChStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	SimulatedBiltCh::SimulatedBiltChStateAllowed.AdditionalMethods

}	//	End of namespace
