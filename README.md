# SimulatedBiltCh

A small Tango class to be used in the framework of the EBS simulator.
It simulates one channel of the Bilt power supply used to power the
static corrector (in DQ magnets, in sextupoles)

## Documentation

Pogo generated HTML pages in **doc_html** folder

## Building and Installation

### Dependencies

The project has no dependencies.

#### Project Dependencies

* Tango release 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* gcc C++11 compliant compiler.
* CMake 

### Build

Instructions on building the project.

CMake skeleton example:

```
cd SimulatedBiltCh
mkdir -p build/<os>
cd build/<os>
cmake ../..
make
```

## License

The code is released under the <GPL3> license and a copy of this license is provided with the code. Full license [here](LICENSE)